<?php

namespace App\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

/**
 * Users Model
 *
 * @method static ShopifyAppClient findFirstById(int $id)
 */
class ShopifyAppClients extends Model
{
    const STATUS_ACTIVE   = 'Y';
    const STATUS_INACTIVE = 'N';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */    
    public $created_at;
    /**
     * @var string
     */
    public $updated_at;
    /**
     * @var string
     */
    public $active;

    public function initialize()
    {
        $this->setSource('shopify_app_clients');
    }

    public function getSource() {
        return 'shopify_app_clients';
    }

    public function validation()
    {
        $validator = new Validation();        

        $validator->add(
            'name',
            new UniquenessValidator([
                'model' => $this,
                'message' => 'Sorry, That shop_name is already taken',
            ])
        );

        return $this->validate($validator);
    }
}
