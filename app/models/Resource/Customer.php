<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 03.07.17
 * Time: 14:57
 */

namespace App\Models\Resource;


class Customer extends ResourceAbstract
{
    /**
     * @var int
     */
    protected $_id;
    /**
     * @var string
     */
    protected $_first_name;
    /**
     * @var string
     */
    protected $_last_name;
    /**
     * @var string
     */
    protected $_created_at;


    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->_first_name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->_last_name;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->_created_at;
    }
}