<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 07.06.17
 * Time: 17:47
 */

namespace App\Models\Resource;


class Order extends ResourceAbstract
{
    /**
     * @var int
     */
    protected $_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }
}