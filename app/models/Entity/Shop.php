<?php
namespace App\Models\Entity;

/**
 * Class Shop
 * @package Shopify\Entity
 */
class Shop extends EntityAbstract
{
    /**
     * Get the configuration of the shop account
     *
     * @return \App\Models\Resource\Shop
     * @throws \App\Library\Shopify\ShopifyException
     */
    public function getConfig()
    {
        // Make an API call
        $response = $this->_request('/admin/shop.json');

        return $this->_parseSingleObject($response, 'shop', '\App\Models\Resource\Shop');
    }
}