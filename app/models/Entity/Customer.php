<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 03.07.17
 * Time: 14:57
 */

namespace App\Models\Entity;

use App\Library\Shopify\ShopifyException;
class Customer  extends EntityAbstract
{
    /**
     * @var array
     */
    protected $_options = [
        
    ];

    /**
     * Returns the list of Customers
     *
     * @return \App\Models\Resource\Customer[]
     */
    public function getCustomers()
    {
        $response = $this->_request('/admin/customers.json', $this->getOptions());

        return $this->_parseMultipleObjects($response, 'Customer', '\App\Models\Resource\Customer');
    }


    /**
     * Returns the number of Customers
     * @return int
     * @throws ShopifyException
     */
    public function getCustomersCount()
    {
        // Make an API call
        $response = $this->_request('/admin/customers/count.json', $this->getOptions());

        //Check if response contains 'application_charge' object
        if (!isset($response['count'])) {
            throw new ShopifyException('Response is not valid. Response dump: ' . var_export($response, true));
        }

        return (int)$response['count'];
    }

    /**
     * Returns the Customer info by it's ID
     *
     * @param int $CustomerId
     * @return \App\Models\Resource\Customer
     */
    public function getCustomer($customerId)
    {
        // Make an API call
        $response = $this->_request('/admin/customers/' . $customerId . '.json', $this->getOptions());

        return $this->_parseSingleObject($response, 'customer', '\App\Models\Resource\Customer');
    }

    /**
     * Create a new Customer
     *
     * @param \App\Models\Resource\Customer $Customer
     * @return \App\Models\Resource\ResourceAbstract
     */
    public function createCustomer(\App\Models\Resource\Customer $customer)
    {
        $response = $this->_request('/admin/customers.json',
            ['customer' => $customer->toArray()], EntityAbstract::METH_POST);

        return $this->_parseSingleObject($response, 'customer', '\App\Models\Resource\Customer');
    }

    /**
     * Update specified Customer with provided data
     *
     * @param \App\Models\Resource\Customer $Customer
     * @return \App\Models\Resource\ResourceAbstract
     * @throws ShopifyException
     */
    public function updateCustomer(\App\Models\Resource\Customer $customer)
    {
        //Check for an ID
        if ((int)$customer->getId() <= 0) {
            throw new ShopifyException('Provided entity should have an ID.');
        }

        $response = $this->_request('/admin/customers/' . $customer->getId() . '.json',
            ['customer' => $customer->toArray()], EntityAbstract::METH_PUT);

        return $this->_parseSingleObject($response, 'customer', '\App\Models\Resource\Customer');
    }

    /**
     * Remove Customer
     *
     * @param int $CustomerId
     */
    public function deleteCustomer($customerId)
    {
        $this->_request('/admin/customers/' . $customerId . '.json', [], EntityAbstract::METH_DELETE);
    }

}