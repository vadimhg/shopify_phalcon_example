<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 07.06.17
 * Time: 17:28
 */

namespace App\Models\Entity;

use App\Library\Shopify\ShopifyException;
class Order extends EntityAbstract
{
    /**
     * @var array
     */
    protected $_options = [
        'ids' => null,
        'limit' => null,
        'page' => null,
        'since_id' => null,
        'created_at_min' => null,
        'created_at_max' => null,
        'updated_at_min' => null,
        'updated_at_max' => null,
        'processed_at_min' => null,
        'processed_at_max' => null,
        'status' => null,
        'financial_status' => null,
        'fulfillment_status' => null,
        'fields' => null,
    ];

    /**
     * Returns the list of Orders
     *
     * @return \App\Models\Resource\Order[]
     */
    public function getOrders()
    {
        $response = $this->_request('/admin/orders.json', $this->getOptions());

        return $this->_parseMultipleObjects($response, 'orders', '\App\Models\Resource\Order');
    }


    /**
     * Returns the number of Orders
     * @return int
     * @throws ShopifyException
     */
    public function getOrdersCount()
    {
        // Make an API call
        $response = $this->_request('/admin/orders/count.json', $this->getOptions());

        //Check if response contains 'application_charge' object
        if (!isset($response['count'])) {
            throw new ShopifyException('Response is not valid. Response dump: ' . var_export($response, true));
        }

        return (int)$response['count'];
    }

    /**
     * Returns the Order info by it's ID
     *
     * @param int $OrderId
     * @return \App\Models\Resource\Order
     */
    public function getOrder($orderId)
    {
        // Make an API call
        $response = $this->_request('/admin/orders/' . $orderId . '.json', $this->getOptions());

        return $this->_parseSingleObject($response, 'order', '\App\Models\Resource\Order');
    }

    /**
     * Create a new Order
     *
     * @param \App\Models\Resource\Order $Order
     * @return \App\Models\Resource\ResourceAbstract
     */
    public function createOrder(\App\Models\Resource\Order $order)
    {
        $response = $this->_request('/admin/orders.json',
            ['order' => $order->toArray()], EntityAbstract::METH_POST);

        return $this->_parseSingleObject($response, 'order', '\App\Models\Resource\Order');
    }

    /**
     * Update specified Order with provided data
     *
     * @param \App\Models\Resource\Order $Order
     * @return \App\Models\Resource\ResourceAbstract
     * @throws ShopifyException
     */
    public function updateOrder(\App\Models\Resource\Order $order)
    {
        //Check for an ID
        if ((int)$order->getId() <= 0) {
            throw new ShopifyException('Provided entity should have an ID.');
        }

        $response = $this->_request('/admin/orders/' . $order->getId() . '.json',
            ['order' => $order->toArray()], EntityAbstract::METH_PUT);

        return $this->_parseSingleObject($response, 'order', '\App\Models\Resource\Order');
    }

    /**
     * Remove Order
     *
     * @param int $OrderId
     */
    public function deleteOrder($orderId)
    {
        $this->_request('/admin/orders/' . $orderId . '.json', [], EntityAbstract::METH_DELETE);
    }

}