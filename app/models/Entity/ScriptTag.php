<?php
namespace App\Models\Entity;


use App\Library\Shopify\ShopifyException;

class ScriptTag extends EntityAbstract
{
    /**
     * @var array
     */
    protected $_options = [
        'limit' => null,
        'page' => null,
        'since_id' => null,
        'created_at_min' => null,
        'created_at_max' => null,
        'updated_at_min' => null,
        'updated_at_max' => null,
        'src' => null,
        'fields' => null,
    ];

    /**
     * Get a list of all script tags
     *
     * @return \App\Models\Resource\ScriptTag[]
     */
    public function getScriptTags()
    {
        $response = $this->_request('/admin/script_tags.json', $this->getOptions());
        return $this->_parseMultipleObjects($response, 'script_tags', '\App\Models\Resource\ScriptTag');
    }

    /**
     * Count script tags with given URL
     *
     * @return int
     * @throws ShopifyException
     */
    public function getScriptTagsCount()
    {
        $response = $this->_request('/admin/script_tags/count.json', $this->getOptions());
        if (!isset($response['count'])) {
            throw new ShopifyException('Response is not valid. Response : ' . var_export($response, true));
        }

        return intval($response['count']);
    }

    /**
     * Get a single script tags by its ID.
     *
     * @param int $scriptTagId
     * @return \App\Models\Resource\ScriptTag
     */
    public function getScriptTag($scriptTagId)
    {
        $response = $this->_request('/admin/script_tags/' . $scriptTagId . '.json', $this->getOptions());
        return $this->_parseSingleObject($response, 'script_tag', '\App\Models\Resource\ScriptTag');
    }

    /**
     * Create a new script tag
     *
     * @param \App\Models\Resource\ScriptTag $scriptTag
     * @return \App\Models\Resource\ScriptTag
     */
    public function createScriptTag(\App\Models\Resource\ScriptTag $scriptTag)
    {
        $response = $this->_request('/admin/script_tags.json', ['script_tag' => $scriptTag->toArray()], EntityAbstract::METH_POST);
        return $this->_parseSingleObject($response, 'script_tag', '\App\Models\Resource\ScriptTag');
    }

    /**
     * Update a script tag
     *
     * @param \App\Models\Resource\ScriptTag $scriptTag
     * @return \App\Models\Resource\ResourceAbstract
     * @throws ShopifyException
     */
    public function updateScriptTag(\App\Models\Resource\ScriptTag $scriptTag)
    {
        if (intval(($scriptTag->getId())) <= 0) {
            throw new ShopifyException('Script Tag resource should have an ID.');
        }

        $response = $this->_request('/admin/script_tags/' . $scriptTag->getId() . '.json',
            ['script_tag' => $scriptTag->toArray()], EntityAbstract::METH_PUT);
        return $this->_parseSingleObject($response, 'script_tag', '\App\Models\Resource\ScriptTag');
    }

    /**
     * Remove an existing script tag from a shop
     *
     * @param int $scriptTagId
     * @return void
     */
    public function deleteScriptTag($scriptTagId)
    {
        $this->_request('/admin/script_tags/' . $scriptTagId . '.json', [], EntityAbstract::METH_DELETE);
    }

}