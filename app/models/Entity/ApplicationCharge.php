<?php
namespace App\Models\Entity;

/**
 * Class ApplicationCharge
 * @link http://docs.shopify.com/api/applicationcharge
 * @package Shopify\Entity
 */
class ApplicationCharge extends EntityAbstract
{


    /**
     * Options list
     * @var array
     */
    protected $_options = [
        'since_id' => null,
        'fields' => null,
    ];

    /**
     * Create a new one-time application charge.
     *
     * @param \App\Models\Resource\ApplicationCharge $applicationCharge
     * @return \App\Models\Resource\ApplicationCharge
     */
    public function createCharge(\App\Models\Resource\ApplicationCharge $applicationCharge)
    {

        // Make an API call
        $response = $this->_request('/admin/application_charges.json', ['application_charge' => $applicationCharge->toArray()], ApplicationCharge::METH_POST);

        return $this->_parseSingleObject($response, 'application_charge', '\App\Models\Resource\ApplicationCharge');
    }


    /**
     * Retrieve one-time application charge
     *
     * @param int $chargeId
     * @return \App\Models\Resource\ApplicationCharge
     * @throws \App\Library\Shopify\ShopifyException
     */
    public function getCharge($chargeId)
    {
        // Make an API call
        $response = $this->_request('/admin/application_charges/' . $chargeId . '.json');

        return $this->_parseSingleObject($response, 'application_charge', '\App\Models\Resource\ApplicationCharge');
    }


    /**
     * All charges that have been requested are retrieved by this request.
     *
     * @throws \App\Library\Shopify\ShopifyException
     * @return \App\Models\Resource\ApplicationCharge[]
     */
    public function getChargesList()
    {

        // Make an API call
        $response = $this->_request('/admin/application_charges.json', $this->getOptions());

        return $this->_parseMultipleObjects($response, 'application_charges', '\App\Models\Resource\ApplicationCharge');
    }


    /**
     * Activate a previously accepted one-time application charge.
     *
     * @param \App\Models\Resource\ApplicationCharge $charge
     * @return void
     */
    public function activateCharge(\App\Models\Resource\ApplicationCharge $charge)
    {
        $this->_request('/admin/application_charges/' . $charge->getId() . '/activate.json',
            ['application_charge' => $charge->toArray()], EntityAbstract::METH_POST);
    }
}