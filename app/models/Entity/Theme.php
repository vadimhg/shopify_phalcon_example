<?php

namespace App\Models\Entity;

use App\Library\Shopify\ShopifyException;

class Theme extends EntityAbstract
{

    /**
     * @var array
     */
    protected $_options = [
        'fields' => null,
    ];

    /**
     * Returns list of a shop's themes.
     *
     * @return \App\Models\Resource\Theme[]
     * @throws ShopifyException
     */
    public function getThemes()
    {
        $response = $this->_request('/admin/themes.json', $this->getOptions());

        return $this->_parseMultipleObjects($response, 'themes', '\App\Models\Resource\Theme');
    }

    /**
     * Return a single theme
     *
     * @param int $themeId
     * @return \App\Models\Resource\Theme
     * @throws ShopifyException
     */
    public function getTheme($themeId)
    {
        $response = $this->_request('/admin/themes/' . $themeId . '.json', $this->getOptions());

        return $this->_parseSingleObject($response, 'theme', '\App\Models\Resource\Theme');
    }

    /**
     * Create a new theme
     *
     * @param \App\Models\Resource\Theme $theme
     * @return \App\Models\Resource\Theme
     * @throws ShopifyException
     */
    public function createTheme(\App\Models\Resource\Theme $theme)
    {
        $response = $this->_request('/admin/themes.json', ['theme' => $theme->toArray()], EntityAbstract::METH_POST);

        return $this->_parseSingleObject($response, 'theme', '\App\Models\Resource\Theme');
    }

    /**
     * Update existing theme
     *
     * @param \App\Models\Resource\Theme $theme
     * @return \App\Models\Resource\Theme
     * @throws ShopifyException
     */
    public function updateTheme(\App\Models\Resource\Theme $theme)
    {

        //Check for ID
        if ((int)$theme->getId() <= 0) {
            throw new ShopifyException('Theme should have ID.');
        }

        $response = $this->_request('/admin/themes/' . $theme->getId() . '.json', ['theme' => $theme->toArray()], EntityAbstract::METH_PUT);

        return $this->_parseSingleObject($response, 'theme', '\App\Models\Resource\Theme');
    }

    /**
     * Delete existing theme
     * @param int $themeId
     * @return \App\Models\Resource\Theme
     * @throws ShopifyException
     */
    public function deleteTheme($themeId)
    {
        $themeData = $this->_request('/admin/themes/' . $themeId . '.json', [], EntityAbstract::METH_DELETE);


        $themeObject = new \App\Models\Resource\Theme();
        $themeObject->fillObjectFromArray($themeData);

        return $themeObject;
    }
}