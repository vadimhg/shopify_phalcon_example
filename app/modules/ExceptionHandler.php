<?php
namespace App\Modules;

use Phalcon\Di;
use App\Library\PHPMailer\PHPMailer;

class ExceptionHandler
{
    protected
        $_renderView,
        $_renderException,
        $_exceptionRenderType,
        $_exception;

    const PHALCON_EXCEPTION_TYPE = 'PHALCON';
    const KOHANA_EXCEPTION_TYPE = 'KOHANA';

    function __construct(){
        $this->setRenderView('show500');
        $this->setExceptionRenderType(ExceptionHandler::KOHANA_EXCEPTION_TYPE);
        $this->setRenderException(APP_DEVELOPMENT==APPLICATION_ENV);
    }

    public function setRenderView($renderView){ //false if do not need render
        $this->_renderView = $renderView;
        return $this;
    }

    public function setExceptionRenderType($exceptionRenderType){
        $this->_exceptionRenderType = $exceptionRenderType;
        return $this;
    }

    public function setException(\Exception $exception){
        $this->_exception = $exception;
        return $this;
    }

    public function setRenderException($renderException){
        $this->_renderException = $renderException;
        return $this;
    }

    public function handle()
    {
        if(isset($this->_exception)) {
            if($this->_renderException) { //RENDER EXCEPTION ON SCREEN
                $this->buildExceptionHtml();
                $this->setRenderView(false);
            }
            else { //capture the exception info and saving to file
                ob_start(); 
                $this->buildExceptionHtml();
                $contents = ob_get_contents();
                ob_end_clean();
                $pathToFile = $this->saveToFile($contents);
                $this->sendEmail($pathToFile);
            }
            if($this->_renderView!==false){
                $this->renderErrorView();
            }
        }
    }

    protected function buildExceptionHtml(){
        switch ($this->_exceptionRenderType) {
            case ExceptionHandler::PHALCON_EXCEPTION_TYPE:
                $debug = new \Phalcon\Debug();
                $debug->onUncaughtException($this->_exception);
            break;
            case ExceptionHandler::KOHANA_EXCEPTION_TYPE:
                echo (new \App\Modules\Custom\CustomException)->setView($this->getViewClass())->handle(($this->_exception));
            break;
        }

    }

    public function saveToFile(&$content)
    {
        $nameFile = date("Ymdhis") . "_" . rand(1, 100) . ".html";
        $fp = fopen(DOCROOT .'public/exceptions/' . $nameFile, 'w');
        fwrite($fp, $content);
        fclose($fp);
        return "/public/exceptions/" . $nameFile;
    }

    public function getViewObject()
    {
        return Di::getDefault()->get('view_new');
    }

    public function getMailTemplate($path)
    {
        $view = $this->getViewObject();
        $view->disableLevel([\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT => true]);
        return $view->getRender('mail', 'exception', ['path'=>$path, 'curserver'=>'http://'.$_SERVER['HTTP_HOST'], 'text' => $this->_exception->getMessage()]);
    }

    public function sendEmail($path){
        $config = new \Phalcon\Config\Adapter\Php(APP_PATH ."config/config.admin.php");
        $mail = new PHPMailer; // todo: put it in DI
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->SMTPAuth = true;
        $mail->Username = $config->username;
        $mail->Password = $config->password;
        //$mail->SMTPDebug = 1;
        $mail->CharSet = $config->charset;
        $mail->Host = $config->host;
        $mail->Port = $config->port;
        $mail->SMTPSecure = $config->security;
        $mail->SetFrom($config->email, $config->from);
        if (isset($_SERVER['HTTP_HOST'])) {$mail->Subject = 'ERRORS ON ' . $_SERVER['HTTP_HOST'];} else {$mail->Subject = 'Error on SERVER';}
        foreach ($config->adminemail as $mailAddress) {$mail->AddAddress($mailAddress);}
        $mail->Body = $this->getMailTemplate($path);
        $mail->Send();        
    }



    public function renderErrorView(){
        $view = $this->getViewObject();
        echo $view->getRender('errors', $this->_renderView);
    }
}
