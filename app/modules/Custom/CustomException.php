<?php
namespace App\Modules\Custom;
class CustomException extends \Exception {

	/**
	 * @var  array  PHP error code => human readable name
	 */
	public static $php_errors = array(
		E_ERROR              => 'Fatal Error',
		E_USER_ERROR         => 'User Error',
		E_PARSE              => 'Parse Error',
		E_WARNING            => 'Warning',
		E_USER_WARNING       => 'User Warning',
		E_STRICT             => 'Strict',
		E_NOTICE             => 'Notice',
		E_RECOVERABLE_ERROR  => 'Recoverable Error',
	);

	/**
	 * @var  string  error rendering view
	 */
	public $error_view = 'kohana';

	/**
	 * @var  string  error folder view
	 */
	
	public $error_folder = 'exceptions';
	/**
	 * @var  string  error view content type
	 */
	public $error_view_content_type = 'text/html';	
	
	protected $view;

	/**
	 * Inline exception handle, displays the error message, source of the
	 * exception, and the stack trace of the error.
	 *
	 * @uses    Kohana_Exception::text
	 * @param   object   exception object
	 * @return  boolean
	 */
	public function handle(\Exception $e)
	{
		try
		{

			$code    = $e->getCode();
			
			// Get the exception backtrace
			$trace = $e->getTrace();

			if ($e instanceof \ErrorException)
			{
				if (isset(CustomException::$php_errors[$code]))
				{
					// Use the human-readable error name
					$code = CustomException::$php_errors[$code];
				}

				if (version_compare(PHP_VERSION, '5.3', '<'))
				{
					// Workaround for a bug in ErrorException::getTrace() that exists in
					// all PHP 5.2 versions. @see http://bugs.php.net/bug.php?id=45895
					for ($i = count($trace) - 1; $i > 0; --$i)
					{
						if (isset($trace[$i - 1]['args']))
						{
							// Re-position the args
							$trace[$i]['args'] = $trace[$i - 1]['args'];

							// Remove the args
							unset($trace[$i - 1]['args']);
						}
					}
				}
			}
			if (isset($this->view)){
				$this->view->disableLevel([\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT => true]);
				return $this->view->getRender($this->error_folder, $this->error_view, [
					'error_id'=>uniqid('error'), 'code' => $code, 'type' => get_class($e),
					'message' => htmlspecialchars($e->getMessage()), 'file' => $e->getFile(), 'line' => $e->getLine(),
					'trace' => $trace, 'debug'=>new Debug()
				]);
			} else {
				return CustomException::text($e);
			}
		}
		catch (\Exception $e){			
			return CustomException::text($e);
		}
	}

	
	public function setView(\Phalcon\Mvc\View $view){
		$this->view = $view;
		return $this;
	}
	/**
	 * Get a single line of text representing the exception:
	 *
	 * Error [ Code ]: Message ~ File [ Line ]
	 *
	 * @param   object  \Exception
	 * @return  string
	 */
	public static function text(\Exception $e)
	{
		return sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($e), $e->getCode(), strip_tags($e->getMessage()), $e->getFile(), $e->getLine());
	}

} // End Kohana_Exception
