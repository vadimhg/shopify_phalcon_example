<?php
function UncachedErrorsCheck( ) {
    $lasterror = error_get_last();
    switch ($lasterror['type'])
    {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
            $loader = new \Phalcon\Loader();
            $loader
                ->registerNamespaces([
                    'App\Library' => APP_PATH.'library/',
                    'App\Modules' => APP_PATH.'modules/'
                ])->register();

            (new \App\Modules\ExceptionHandler)->setException((new \Exception($lasterror['message'])))->handle();
    }
}
register_shutdown_function('UncachedErrorsCheck');