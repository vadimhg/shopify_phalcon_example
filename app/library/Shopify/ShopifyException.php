<?php
namespace App\Library\Shopify;

/**
 * Shopify Exception class
 * @package Shopify
 */
class ShopifyException extends \Exception
{

}