<?php

namespace App\Library;

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class BreadCrumbs extends Component
{
    private $_route, $_controller, $_action;

    /**
     * Builds header menu with left and right items
     *
     * @return array
     */

    function __construct(){
        $this->_route = [];
        $this->_controller=$this->router->getControllerName();
        $this->_action = $this->router->getActionName();
    }

    public function getRoute()
    {
        return $this->_route;
    }

    public function addRoute($caption, $controller='index', $action='index'){
        array_push($this->_route, ['caption'=>$caption, 'controller'=>$controller, 'action'=>$action]);
    }

    public function getListTag($element){
        $active = $element['controller']==$this->_controller&&$element['action']==$this->_action;
        return $this->tag->tagHtml('li', ['class'=>'breadcrumb-item'.($active?' active':'')]).($active?$element['caption']:$this->tag->linkTo($this->url->get(['for'=> 'front.full', 'controller'=> $element['controller'], 'action' => $element['action']]), $element['caption'])).$this->tag->tagHtmlClose('li');
    }
}
