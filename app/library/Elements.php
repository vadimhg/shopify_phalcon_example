<?php

namespace App\Library;

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Component
{
    private $leftMenu = [
                'test' => [
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-bomb'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Test LINK',
                    'route' => ['controller'=>'test', 'action'=>'index'],
                    'sub'=> false,
                    'link-class'=>'nav-link'
                ],
                'dashboard' => [
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-dashboard'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Dashboard',
                    'route' => ['controller'=>'dashboard', 'action'=>'index'],
                    'sub'=> false,
                    'link-class'=>'nav-link'
                ],
                'tables'=>[
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-sitemap'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Tables',
                    'route' => false,
                    'link-class'=>'nav-link nav-link-collapse collapsed',
                    'sub'=>[
                        'orders'=>[
                            'font-icon-class'=>['fa', 'fa-fw', 'fa-money'],//http://fontawesome.io/cheatsheet/
                            'caption' => 'Orders',
                            'route' => ['controller'=>'orders', 'action'=>'index'],
                            'link-class'=>'',
                            'sub'=> false
                        ],
                        'products'=>[
                            'font-icon-class'=>['fa', 'fa-fw', 'fa-table'],//http://fontawesome.io/cheatsheet/
                            'caption' => 'Products',
                            'route' => ['controller'=>'products', 'action'=>'index'],
                            'link-class'=>'',
                            'sub'=> false
                        ]/*,
                        'pages'=>[
                            'font-icon-class'=>['fa', 'fa-fw', 'fa-newspaper-o'],//http://fontawesome.io/cheatsheet/
                            'caption' => 'Pages',
                            'route' => ['controller'=>'pages', 'action'=>'index'],
                            'link-class'=>'',
                            'sub'=> false
                        ]*/
                    ]
                ],
                'customers'=> [
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-users'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Customers',
                    'route' => ['controller'=>'customers', 'action'=>'index'],
                    'sub'=> false,
                    'link-class'=>'nav-link'
                ],
                'notifications'=> [
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-envelope'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Notifications',
                    'route' => ['controller'=>'notifications', 'action'=>'index'],
                    'sub'=> false,
                    'link-class'=>'nav-link'
                ],
                'settings'=> [
                    'font-icon-class'=>['fa', 'fa-fw', 'fa-wrench'],//http://fontawesome.io/cheatsheet/
                    'caption' => 'Settings',
                    'route' => false,
                    'sub'=> [
                        'settings1'=>[
                            'font-icon-class'=>['fa', 'fa-fw', 'fa-wrench'],//http://fontawesome.io/cheatsheet/
                            'caption' => 'Settings1',
                            'route' => ['controller'=>'settings', 'action'=>'index'],
                            'link-class'=>'',
                            'sub'=> false
                        ],
                        'settings2'=>[
                            'font-icon-class'=>['fa', 'fa-fw', 'fa-wrench'],//http://fontawesome.io/cheatsheet/
                            'caption' => 'Settings2',
                            'route' => ['controller'=>'settings', 'action'=>'index'],
                            'link-class'=>'',
                            'sub'=> false
                        ]
                    ],
                    'link-class'=>'nav-link nav-link-collapse collapsed'
                ],


    ],
    $settingsType = [
        'first-level-link'=>[
            'link-class' => []
        ],
        'first-level-collapced'=>[
            'link-class' => []
        ]
    ];


    /**
     * Builds header menu with left and right items
     *
     * @return string
     */
    public function getLeftMenu()
    {        
        return $this->leftMenu;
    }
    
    protected function getElementByKey($key1, $key2 = false, $key3 = false){
        return $key3===false?($key2===false?$this->leftMenu[$key1]:$this->leftMenu[$key1][$key2]):$this->leftMenu[$key1][$key2][$key3];
    }

    public function getListTag($content, $params = []){
        return $this->tag->tagHtml('li', $params).$content.$this->tag->tagHtmlClose('li');
    }

    public function getLinkTag($key, array &$navElement){
        if($navElement['route']===false)
            return $this->tag->linkTo(["#collapse".ucfirst($key), $this->getFontIcon($navElement).$navElement['caption'], "local" => false, 'class'=>$navElement['link-class'], 'data-toggle'=>'collapse']);
        else
            return $this->tag->linkTo([$this->getLinkUrl($navElement), $this->getFontIcon($navElement).$navElement['caption'], 'class'=>$navElement['link-class']]);
    }

    private function getLinkUrl(array &$navElement){
        return $this->url->get(['for'=> 'front.full', 'controller'=> $navElement['route']['controller'], 'action' => $navElement['route']['action']]);
    }

    private function getFontIcon(array &$navElement){
        return $navElement['font-icon-class']===false?''
            :($this->tag->tagHtml('i', ['class'=>implode(' ', $navElement['font-icon-class'])]).$this->tag->tagHtmlClose('i').' ');
    }
}
