<?php

namespace App\Plugins\Acl;

use App\Plugins\Acl\Resource\ResourceInterface;

class Resource implements ResourceInterface
{
    private $privateResources = [
        'dashboard' =>  ['index'],
        'datatables' =>  ['products', 'orders'],
        'customers' => ['index'],
        'ajax' =>  ['index'],
        'messages' =>  ['index'],
        'orders' => ['index'],
        'products' => ['index'],
        'installing_app' => ['index'],
        'installing_ajax' => ['step1', 'step2'],
        'notifications' => ['index', 'message'],
        'test' => ['index']
    ];

    private $publicResources = [
        'index'      => ['index'],
        'auth'      => ['index', 'login', 'link', 'logout'],
        'errors'     => ['show401', 'show404', 'show500'],
        'contact'    => ['index', 'send'],
        'webhook'   => ['orders_create', 'app_uninstalled', 'shop_update'],
    ];

    private $allResources;

    public function getPrivateResources()
    {
        return $this->privateResources;
    }

    public function getPublicResources()
    {
        return $this->publicResources;
    }

    public function hasResource($name)
    {
        $allResources = $this->getAllResources();

        return isset($allResources[$name]);
    }

    public function hasAccess($resource, $access)
    {
        $allResources = $this->getAllResources();

        return isset($allResources[$resource]) && in_array($access, $allResources[$resource]);
    }

    public function getAllResources()
    {
        if (!empty($this->allResources)) {
            return $this->allResources;
        }

        $allResources = [];

        foreach ($this->privateResources as $resource => $accessList) {
            if (!is_array($accessList)) {
                $accessList = [$accessList];
            }

            if (!isset($allResources[$resource])) {
                $allResources[$resource] = [];
            }

            $allResources[$resource] = array_merge($allResources[$resource], $accessList);
        }

        foreach ($this->publicResources as $resource => $accessList) {
            if (!is_array($accessList)) {
                $accessList = [$accessList];
            }

            if (!isset($allResources[$resource])) {
                $allResources[$resource] = [];
            }

            $allResources[$resource] = array_merge($allResources[$resource], $accessList);
        }

        $this->allResources = $allResources;
        return $allResources;
    }
}
