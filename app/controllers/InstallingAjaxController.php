<?php
namespace App\Controllers;
class InstallingAjaxController extends ControllerAjax
{

    public function initialize()
    {
        parent::initialize();
    }

    public function step1Action()
    {
        try {
            if($this->session->has('shop') && $this->session->has('access_token')){
                $shopName = $this->session->get('shop', 'string');
                $shopifyAppClient = \App\Models\ShopifyAppClients::findFirstByName($shopName);
                if(!$shopifyAppClient) {
                    $shopifyAppClient = new \App\Models\ShopifyAppClients();
                    $shopifyAppClient->created_at = date('Y-m-d H:i:s', time());
                }
                $shopifyAppClient->active = 1;
                $shopifyAppClient->name = $shopName;
                $shopifyAppClient->updated_at = date('Y-m-d H:i:s', time());
                if($shopifyAppClient->save()===false) {
                    $messages = '';
                    foreach ($shopifyAppClient->getMessages() as $mess)
                        $messages .= $mess->__toString();
                    throw new \Exception($messages);
                }
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(['success'=>true]);
        } catch (\Exception $exception) {
            (new \App\Modules\ExceptionHandler)
                ->setException($exception)
                ->setRenderView(false)
                ->setRenderException(false)
                ->handle();
            $this->response->setStatusCode(500, "Error");
            $this->response->setJsonContent(['success'=>false]);
        }
        return $this->response;
    }

    public function step2Action()
    {
        try {
            $webhooksList = new \Phalcon\Config\Adapter\Php(APP_PATH . "config/webhooks.php");
            $webHooks = (new \App\Models\Entity\WebHook())->setApplication($this->shopify_application)->getWebHooks();
            //todo: Сделать через update. Но пока и так сойдет https://pp.userapi.com/c621122/v621122274/474a/udsakTueiFI.jpg
            foreach ($webHooks as $webHook) {
                (new \App\Models\Entity\WebHook())->setApplication($this->shopify_application)->deleteWebHook($webHook->getId());
            }
            foreach($webhooksList as $webhookItem) {               
                $webHookResourse = (new \App\Models\Resource\WebHook())->fillFromConnfigItem($webhookItem);
                (new \App\Models\Entity\WebHook())->setApplication($this->shopify_application)->createWebHook($webHookResourse);
            }
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent(['success'=>true]);
        } catch (\Exception $exception) {
            (new \App\Modules\ExceptionHandler)
                ->setException($exception)
                ->setRenderView(false)
                ->setRenderException(false)
                ->handle();
            $this->response->setStatusCode(500, "Error");
            $this->response->setJsonContent(['success'=>false]);
        }
        return $this->response;
    }
}

