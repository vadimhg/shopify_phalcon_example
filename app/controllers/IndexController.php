<?php

namespace App\Controllers;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Welcome');
        if(!$this->dispatcher->getPreviousActionName()){
            $this->assets->collection("headerCSS")
                ->addCss("/vendor/bootstrap/css/bootstrap.min.css?ver=".APP_VERSION) //Bootstrap core CSS
                ->addCss("/vendor/font-awesome/css/font-awesome.min.css?ver=".APP_VERSION) //Custom fonts for this template
                ->addCss("/css/sb-admin.css?ver=".APP_VERSION);
            $this->assets->collection("footerJS")
                ->addJs("/vendor/jquery/jquery.min.js")//Bootstrap core JavaScript
                ->addJs("/vendor/tether/tether.min.js")
                ->addJs("/vendor/bootstrap/js/bootstrap.min.js");
        }
    }

    public function indexAction()
    {        
        $this->view->shop = $this->request->get("shop", "string");
    }
}
