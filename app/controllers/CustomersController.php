<?php

namespace App\Controllers;

/**
 * CustomersController
 */
class CustomersController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Customers List');
        $this->breadcrumbs->addRoute('Customers', 'customers');
        $this->assets->collection("footerJS")
            ->addJs("/vendor/datatables/jquery.dataTables.js")
            ->addJs("/js/customers-admin.js?ver=".APP_VERSION);
        parent::initialize();
    }

    public function indexAction()
    {
        
    }
}
