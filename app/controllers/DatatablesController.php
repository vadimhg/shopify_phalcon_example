<?php

namespace App\Controllers;
use Phalcon\Mvc\Controller;
class DatatablesController extends ControllerAjax
{

    protected
        $_options = [];

    public function initialize()
    {
        parent::initialize();
        $this->_options = [
            'page' => ceil($this->request->get('iDisplayStart', 'int')/$this->request->get('iDisplayLength', 'int')) + 1,
            'limit' => $this->request->get('iDisplayLength', 'int')
        ];
    }

    public function productsAction()
    {
        try {
            $productEntity = (new \App\Models\Entity\Product())->setApplication($this->shopify_application);
            $productCount = $productEntity->getProductsCount();
            $aaData = [];
            if($productCount) {
                $productList = $productEntity->setOptions($this->_options)->getProducts();
                foreach($productList as $productResourse){
                    if($productResourse instanceof \App\Models\Resource\Product) {
                        $variants = $productResourse->getVariants();
                        $onlyOneVariant = count($variants)===1;
                        array_push($aaData, (object)[
                            "id" => $productResourse->getId(),
                            "title" => $productResourse->getTitle(),
                            "price" => $onlyOneVariant?$variants[0]['price']:"-",
                            "sku" => $onlyOneVariant?$variants[0]['sku']:"-",
                            "position" => $onlyOneVariant?$variants[0]['position']:"-",
                            "grams" => $onlyOneVariant?$variants[0]['grams']:"-",
                            "created_at" => $productResourse->getCreatedAt(),
                            "updated_at" => $productResourse->getUpdatedAt(),
                            "variants" => $productResourse->getVariants()
                        ]);
                    }
                }
            }

            $response = [
                'iTotalDisplayRecords' => $productCount,
                'aaData' => $aaData,
                'sEcho' => $this->request->get('sEcho')
            ];
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent($response);
        } catch (\App\Library\Shopify\ShopifyException $exception) {
            (new \App\Modules\ExceptionHandler)
                ->setException($exception)
                ->setRenderView(false)
                ->setRenderException(false)
                ->handle();
            $this->response->setStatusCode(500, "Error");
            $this->response->setJsonContent(new \stdClass());
        }
        return $this->response;
    }

    public function ordersAction()
    {
/*

      "email": "bob.norman@hostmail.com",
      "closed_at": null,
      "created_at": "2008-01-10T11:00:00-05:00",
      "updated_at": "2008-01-10T11:00:00-05:00",
      "number": 1,
      "note": null,
      "token": "b1946ac92492d2347c6235b4d2611184",
      "gateway": "authorize_net",
      "test": false,
      "total_price": "409.94",
      "subtotal_price": "398.00",
      "total_weight": 0,
      "total_tax": "11.94",
      "taxes_included": false,
      "currency": "USD",
      "financial_status": "authorized",
      "confirmed": false,
      "total_discounts": "0.00",
      "total_line_items_price": "398.00",
      "cart_token": "68778783ad298f1c80c3bafcddeea02f",
      "buyer_accepts_marketing": false,
      "name": "#1001",
      "referring_site": "http:\/\/www.otherexample.com",
      "landing_site": "http:\/\/www.example.com?source=abc",
      "cancelled_at": null,
      "cancel_reason": null,
      "total_price_usd": "409.94",
      "checkout_token": "bd5a8aa1ecd019dd3520ff791ee3a24c",
      "reference": "fhwdgads",
      "user_id": null,
      "location_id": null,
      "source_identifier": "fhwdgads",
      "source_url": null,
      "processed_at": "2008-01-10T11:00:00-05:00",
      "device_id": null,
      "browser_ip": null,
      "landing_site_ref": "abc",
      "order_number": 1001,*/


        try {
            $ordersEntity = (new \App\Models\Entity\Order())->setApplication($this->shopify_application);
            $ordersCount = $ordersEntity->getOrdersCount();
            $aaData = [];
            if($ordersCount) {
                $ordersctList = $ordersEntity->setOptions($this->_options)->getOrders();
                foreach($ordersctList as $orderResourse){
                    if($orderResourse instanceof \App\Models\Resource\Order) {
                        array_push($aaData, (object)[

                        ]);
                    }
                }
            }
            $response = [
                'iTotalDisplayRecords' => $ordersCount,
                'aaData' => $aaData,
                'sEcho' => $this->request->get('sEcho')
            ];
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent($response);
        } catch (\App\Library\Shopify\ShopifyException $exception) {
            (new \App\Modules\ExceptionHandler)
                ->setException($exception)
                ->setRenderView(false)
                ->setRenderException(false)
                ->handle();
            $this->response->setStatusCode(500, "Error");
            $this->response->setJsonContent(new \stdClass());
        }
        return $this->response;
    }

    public function customersAction()
    {
        try {
            $customersEntity = (new \App\Models\Entity\Customer())->setApplication($this->shopify_application);
            $customersCount = $customersEntity->getCustomersCount();
            $aaData = [];
            if($customersCount) {
                $ordersctList = $customersEntity->setOptions($this->_options)->getCustomers();
                foreach($ordersctList as $customerResourse){
                    if($customerResourse instanceof \App\Models\Resource\Customer) {
                        array_push($aaData, (object)[
                            "id" => $customerResourse->getId(),
                            "first_name" => $customerResourse->getFirstName(),
                            "last_name" => $customerResourse->getLastName(),
                            "created_at" => $customerResourse->getCreatedAt(),
                        ]);
                    }
                }
            }
            $response = [
                'iTotalDisplayRecords' => $customersCount,
                'aaData' => $aaData,
                'sEcho' => $this->request->get('sEcho')
            ];
            $this->response->setStatusCode(200, "OK");
            $this->response->setJsonContent($response);
        } catch (\App\Library\Shopify\ShopifyException $exception) {
            (new \App\Modules\ExceptionHandler)
                ->setException($exception)
                ->setRenderView(false)
                ->setRenderException(false)
                ->handle();
            $this->response->setStatusCode(500, "Error");
            $this->response->setJsonContent(new \stdClass());
        }
        return $this->response;
    }
}
