<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;

class ControllerAjax extends Controller
{

    public function beforeExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        if(!$this->request->isAjax()) {
            $this->response->redirect($this->url->get(['for'=> 'front.full', 'controller'=> 'index', 'action' => 'index']), false, 301);
            return false;
        }
    }


    public function initialize()
    {
        if($this->session->has('shop') && $this->session->has('access_token')){
            $this->shopify_application->setAccessToken($this->session->get('access_token'))->setBaseUri('https://' . $this->session->get('shop'));
        }
        $this->view->disable();
        
    }
}
