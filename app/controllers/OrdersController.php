<?php

namespace App\Controllers;


class OrdersController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Orders');
        $this->breadcrumbs->addRoute('Orders', 'orders');
        $this->assets->collection("footerJS")
            ->addJs("/vendor/datatables/jquery.dataTables.js")
            ->addJs("/js/orders-admin.js?ver=".APP_VERSION);
    }

    public function indexAction()
    {

    }

}

