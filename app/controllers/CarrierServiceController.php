<?php
namespace App\Controllers;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
class CarrierServiceController extends \Phalcon\Mvc\Controller
{
    protected  $_logger, $_responce;
    public function beforeExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $this->_logger = new FileAdapter("app/logs/".$dispatcher->getActionName().".log");
        $this->_logger->begin();
    }

    public function initialize()
    {
        $this->_responce = [];
        $this->_logger = new \Phalcon\Logger\Adapter\File("app/logs/test.log");
    }

    public function indexAction()
    {
        $this->_logger->info("Carrier Service started");
        $this->addRates([
            "service_name"=> 'service_name TEST',
            "service_code"=> 'service_code',
            "total_price"=> (float)1234,
            "currency"=> 'ua',
            "min_delivery_date"=> "2018-04-12 14:48:45 -0400",
            "max_delivery_date"=> "2018-04-13 14:48:45 -0400",
        ]);
        $this->response->setStatusCode(200, "OK");
        $this->response->setJsonContent($this->_responce);
    }    

    public function afterExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher)
    {
        $this->_logger->commit();
    }


    protected function addRates($item){
        $this->_result['rates'][] = $item;
    }
}

