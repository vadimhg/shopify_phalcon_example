<?php

namespace App\Controllers;


class OrdersController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Pages');
        $footerCollection = $this->assets->collection("footer");
        $footerCollection->addJs("/js/pages-admin.js?ver=".APP_VERSION);
    }

    public function indexAction()
    {

    }

}

