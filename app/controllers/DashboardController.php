<?php
namespace App\Controllers;

class DashboardController extends ControllerBase
{

    public function initialize()
    {
        $this->breadcrumbs->addRoute('Dashboard', 'dashboard');
        $this->tag->setTitle('Dashboard');
        parent::initialize();
        $this->assets->collection("footerJS")
            ->addJs("/vendor/jquery-easing/jquery.easing.min.js")//Plugin JavaScript
            ->addJs("/vendor/chart.js/Chart.min.js")
            ->addJs("/vendor/datatables/jquery.dataTables.js")
            ->addJs("/vendor/datatables/dataTables.bootstrap4.js")
            ->addJs("/js/sb-admin.js?ver=".APP_VERSION);//Custom scripts for this template
    }

    public function indexAction()
    {
        
    }
}

