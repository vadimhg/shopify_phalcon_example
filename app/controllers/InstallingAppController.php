<?php
namespace App\Controllers;
class InstallingAppController extends \Phalcon\Mvc\Controller
{

    public function initialize()
    {
        $this->tag->setTitle('Installing');
        $this->assets->collection("headerCSS")
            ->addCss("/vendor/bootstrap/css/bootstrap.min.css")
            ->addCss("/vendor/bootstrap/css/bootstrap-grid.css")
            ->addCss("/css/installing-app.css?vendor=".APP_VERSION)
        ;
        $this->assets->collection("footerJS")
            ->addJs("/vendor/jquery/jquery.min.js")//Bootstrap core JavaScript
            ->addJs("/vendor/tether/tether.min.js")
            ->addJs("/vendor/bootstrap/js/bootstrap.min.js")
            ->addJs("/js/installing-app.js?vendor=".APP_VERSION);
    }
    
    public function indexAction()
    {
        
    }    
    
}

