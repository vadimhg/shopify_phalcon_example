<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 12.06.17
 * Time: 18:35
 */

namespace App\Controllers;


class NotificationsController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->tag->setTitle('Orders');
        $this->breadcrumbs->addRoute('Notifications', 'notifications');
        $footerCollection = $this->assets->collection("footer");
        $footerCollection->addJs("/js/notification-admin.js");
    }

    public function indexAction()
    {

    }

}

