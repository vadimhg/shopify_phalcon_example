<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 03.07.17
 * Time: 18:14
 */

namespace App\Controllers;


class TestController  extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('TEST');
        $this->breadcrumbs->addRoute('TEST', 'test');
        parent::initialize();
        $this->assets->collection("headerJS")->addJs("https://cdn.shopify.com/s/assets/external/app.js");
        //$this->assets->collection("footerJS")->addJs("/js/test-admin.js?ver=".APP_VERSION);
    }

    public function indexAction()
    {
        $this->view->setVar('shop', $this->session->get('shop'))->setVar('SHOPIFY_API_KEY', SHOPIFY_API_KEY);
    }
}