<?php

namespace App\Controllers;

use Phalcon\Mvc\Model\Criteria;
//use Phalcon\Paginator\Adapter\Model as Paginator;
//use App\Forms\ProductsForm;
//use App\Models\Products;

/**
 * ProductsController
 *
 * Manage CRUD operations for products
 */
class ProductsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Manage your products');
        $this->breadcrumbs->addRoute('Products', 'products');        
        parent::initialize();
        $this->assets->collection("footerJS")
            ->addJs("/vendor/datatables/jquery.dataTables.js")
            ->addJs("/js/products-admin.js?ver=".APP_VERSION);//Custom scripts for this template
    }

    /**
     * Shows the index action
     */
    public function indexAction()
    {

    }
}
