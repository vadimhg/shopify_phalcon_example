<?php

namespace App\Controllers;
use \App\Library\Shopify\Application;
use \App\Library\Shopify\OAuth;
/*
 * Кстати, почему это говно делается на PHP?
 * Ответ 1: потому что эта жлобская фирма не хочет нанимать Ruby разработчика!
 * Ответ 2: потому что отдел продаж не разбирается в том что и по каким технологиям делается!
 * */
class AuthController extends ControllerBase
{
    public function indexAction()
    {
        $this->dispatcher->forward(['controller'=>'index', 'action' => 'index']);
    }

    public function logoutAction()
    {
        $this->session->destroy(true);
        $this->flash->success("You logged out");
        $this->dispatcher->forward(['controller'=>'index', 'action' => 'index']);
    }

    public function loginAction()
    {
        $this->session->remove("shop");// SECURITY REASON. We do not rewrite previous session. We need to use new one.
        $this->session->remove("access_token");
        try {
            if($this->request->has("shop") && $this->request->has("code")) {
                $this->shopify_application->setBaseUri('https://' . $this->request->get("shop", "string"));
                $access_token = (new OAuth())->setApplication($this->shopify_application)->generateAccessToken($this->request->get("code", "string"));
                $this->session->set("access_token", $access_token);
                $this->session->set("shop", $this->request->get("shop", "string"));
                if($this->checkShopExistInDB($this->request->get("shop", "string")) && $this->checkIsWebhooksInstaled()) {
                    $this->response->redirect($this->url->get(['for' => 'front.full', 'controller' => 'dashboard', 'action' => 'index']), false, 301);
                }
                else {
                    $this->response->redirect($this->url->get(['for' => 'front.full', 'controller' => 'installing_app', 'action' => 'index']), false, 301);
                }
            } else {
                throw new \App\Library\Shopify\ShopifyException('Bad request');
            }
        } catch (\App\Library\Shopify\ShopifyException $exception) {
            $this->flash->success("Error while trying to login");
            $this->dispatcher->forward(['controller'=>'index', 'action' => 'index']);
        }
    }

    public function linkAction()
    {
        if($this->request->has("shop")) {
            $this->session->destroy();
            $this->shopify_application->setBaseUri('https://'.$this->request->get("shop", "string"));
            $this->response->redirect($this->shopify_application->generateAuthorizationUrl(), false, 301);
        } else {
            $this->flash->error("Please enter shop name");
            $this->dispatcher->forward(['controller'=>'index', 'action' => 'index']);
        }
    }

    protected function checkShopExistInDB($shopName){
        return \App\Models\ShopifyAppClients::findFirstByName($shopName)!==false;
    }

    protected function checkIsWebhooksInstaled(){
        $webhooksList = (array)(new \Phalcon\Config\Adapter\Php(APP_PATH . "config/webhooks.php"));
        $webHooks = (new \App\Models\Entity\WebHook())->setApplication($this->shopify_application)->getWebHooks();
        if(count($webhooksList) != count($webHooks)) return false;
        foreach($webhooksList as $webhookItem) {
            $webHookResourse = (new \App\Models\Resource\WebHook())->fillFromConnfigItem($webhookItem);
            foreach ($webHooks as $webHook){
                if($webHookResourse->isEquals($webHook)){
                    continue 2;
                }
            }
            return false;
        }
        return true;
    }
}
