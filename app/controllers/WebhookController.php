<?php
namespace App\Controllers;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
class WebhookController extends \Phalcon\Mvc\Controller
{
    protected  $_logger;
    public function beforeExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher){
        $this->_logger = new FileAdapter("app/logs/".$dispatcher->getActionName().".log");
        $this->_logger->begin();
    }

    public function initialize(){
        $this->_logger = new \Phalcon\Logger\Adapter\File("app/logs/test.log");
    }
    
    public function orders_createAction(){
        $this->_logger->info("Webhook orders_create started");
    }

    public function app_uninstalledAction(){
        $this->_logger->info("Webhook app_uninstalled started");
    }

    public function shop_updateAction(){
        $this->_logger->info("Webhook shop_update started");
    }

    public function afterExecuteRoute(\Phalcon\Mvc\Dispatcher $dispatcher){
        $this->_logger->commit();
    }

}

