<?php

namespace App\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {   
        $this->tag->prependTitle('Phalcon Shopify Application | ');        
        if($this->session->has('shop') && $this->session->has('access_token')){
            $this->shopify_application->setAccessToken($this->session->get('access_token'))->setBaseUri('https://' . $this->session->get('shop'));
        }
        if(!$this->dispatcher->getPreviousActionName()){
            $this->assets->collection("headerCSS")
                ->addCss("/vendor/bootstrap/css/bootstrap.min.css?ver=".APP_VERSION) //Bootstrap core CSS
                ->addCss("/vendor/font-awesome/css/font-awesome.min.css?ver=".APP_VERSION) //Custom fonts for this template
                ->addCss("/vendor/datatables/dataTables.bootstrap4.css?ver=".APP_VERSION)//Plugin CSS
                ->addCss("/css/sb-admin.css?ver=".APP_VERSION);
            $this->assets->collection("footerJS")
                ->addJs("/vendor/jquery/jquery.min.js")//Bootstrap core JavaScript
                ->addJs("/vendor/tether/tether.min.js")
                ->addJs("/vendor/bootstrap/js/bootstrap.min.js");
        }        
    }

    public function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
        return $this->dispatcher->forward(
            [
                'controller' => $uriParts[0],
                'action'     => $uriParts[1],
                'params'     => $params
            ]
        );
    }
}
