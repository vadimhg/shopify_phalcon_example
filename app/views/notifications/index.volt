{% extends "templates/base.volt" %}
{% block content %}
    <!-- Example Tables Card -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-fw fa-envelope"></i> Notifications List
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>message</th>
                        <th>type</th>
                        <th>date</th>
                        <th>mark</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>id</th>
                        <th>message</th>
                        <th>type</th>
                        <th>date</th>
                        <th>mark</th>
                    </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
{% endblock %}