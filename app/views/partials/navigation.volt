<!-- Navigation -->
<nav id="mainNav" class="navbar static-top navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarLeft" aria-controls="navbarLeft" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{ url(['for': 'front.controller', 'controller': 'dashboard']) }}">Home</a>
    <div class="collapse navbar-collapse" id="navbarLeft">
        {{ partial("partials/sidebar") }}
        <ul class="navbar-nav ml-auto">
            {{ partial("partials/messages") }}
            <li class="nav-item">
                <a class="nav-link" href="{{ url(['for': 'auth.action', 'action': 'logout']) }}"><i class="fa fa-fw fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>
</nav>