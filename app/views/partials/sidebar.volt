<ul class="sidebar-nav navbar-nav">
    {% for key1, nav1 in elements.getLeftMenu() %}
        {{ elements.getListTag(elements.getLinkTag(key1, nav1), ['class':'nav-item']) }}
        {% if nav1['sub'] !==false %}
            <ul class="sidebar-second-level collapse" id="collapse{{ key1 | ucwords }}">
                {% for key2, nav2 in nav1['sub'] %}
                    {{ elements.getListTag(elements.getLinkTag(key2, nav2)) }}
                    {% if nav2['sub'] !==false %}
                        <ul class="sidebar-third-level collapse" id="collapse{{ key2 | ucwords }}">
                            {% for key3, nav3 in nav2['sub'] %}
                                {{ elements.getListTag(elements.getLinkTag(key3, nav3)) }}
                            {% endfor %}
                        </ul>
                    {% endif %}
                {% endfor %}
            </ul>
        {% endif %}
    {% endfor %}
</ul>