<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-chevron-up"></i>
</a>
{{ partial("partials/modal") }}
{% if(assets.exists("footerJS")) %}{{ assets.outputJs("footerJS") }}{% endif %}