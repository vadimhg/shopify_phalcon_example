<!-- Breadcrumbs -->
<ol class="breadcrumb">
    {% for crumb in breadcrumbs.getRoute() %}
    {{ breadcrumbs.getListTag(crumb) }}
    {% endfor %}
</ol>