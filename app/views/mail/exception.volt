{% extends "templates/mail.volt" %}
{% block body %}
<tr>
    <td>
        <p style="display: block; margin: 50px 16px 40px 16px; font: 22px Arial, sans-serif; line-height: 22px; -webkit-text-size-adjust: none;">
            Ошибка на сервисе {{curserver}}</p>
    </td>
</tr>
<tr>
    <td style="padding-bottom: 10px;">
        <p style="color: #333333; font: 16px Arial, sans-serif; line-height: 20px; -webkit-text-size-adjust:none; display: block; padding: 5px 5px 5px 20px;">
            Описание ошибки: {{ text }}<br/><br/><br/>
            <a href="{{curserver}}{{path}}" target="_blank">Перейти к ошибке</a>
        </p>
    </td>
</tr>
{% endblock %}