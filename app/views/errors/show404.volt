<style>
    html, body {
        height: 100%;
    }
</style>
<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css?ver=0.0001" />
<link rel="stylesheet" type="text/css" href="/css/sb-admin.css?ver=0.0001" />
<script type="text/javascript" src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<div class="content-wrapper py-3" style="min-height:100%">
    <div class="container-fluid" style="min-height:100%">
        <div class="jumbotron">
            <h1>Page not found</h1>
            <p>Sorry, you have accessed a page that does not exist or was moved</p>
            <p>{{ link_to('index', 'Home', 'class': 'btn btn-primary') }}</p>
        </div>
    </div>
</div>
