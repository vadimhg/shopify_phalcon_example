<style>
    html, body {
        height: 100%;
    }
</style>
<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css?ver=0.0001" />
<link rel="stylesheet" type="text/css" href="/css/sb-admin.css?ver=0.0001" />
<script type="text/javascript" src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<div class="content-wrapper py-3" style="min-height:100%">
    <div class="container-fluid" style="min-height:100%">
        <div class="jumbotron">
            <h1>Internal Error</h1>
            <p>Something went wrong, if the error continue please contact us</p>
            <p>{{ link_to('index', 'Home', 'class': 'btn btn-primary') }}</p>
        </div>
    </div>
</div>