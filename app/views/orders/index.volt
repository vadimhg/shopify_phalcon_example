{% extends "templates/base.volt" %}
{% block content %}
    <!-- Example Tables Card -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Orders List
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>title</th>
                        <th>price</th>
                        <th>sku</th>
                        <th>position</th>
                        <th>grams</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th>id</th>
                        <th>title</th>
                        <th>price</th>
                        <th>sku</th>
                        <th>position</th>
                        <th>grams</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                    </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
{% endblock %}