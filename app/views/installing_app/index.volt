<div class="container">
    <div class="container-fluid">
        <div class="card card-inverse card-info mb-3">
            <img class="card-img-top img-fluid" src="/img/install/donuts.jpg" alt="Card image cap">
            <div class="card-block">
                <p class="card-text">We are installing this application to your shopify store. Please wait...</p>
            </div>
        </div>
    <div class="row bs-wizard" style="border-bottom:0;">
        <div class="col-xl-3 bs-wizard-step j-install-step-0">
            <div class="text-center bs-wizard-stepnum">Start</div>
            <div class="progress"><div class="progress-bar bg-success"></div></div>
            <a class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"></div>
        </div>
        <div class="col-xl-3 bs-wizard-step j-install-step-1">
            <div class="text-center bs-wizard-stepnum">Step 1</div>
            <div class="progress"><div class="progress-bar bg-success"></div></div>
            <a class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Creating store configuration...</div>
        </div>

        <div class="col-xl-3 bs-wizard-step j-install-step-2"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Step 2</div>
            <div class="progress"><div class="progress-bar bg-success"></div></div>
            <a class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">Installing webhooks on your store</div>
        </div>
        <div class="col-xl-3 bs-wizard-step j-install-step-3"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Finish</div>
            <div class="progress"><div class="progress-bar bg-success"></div></div>
            <a class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center"> Ready. We will redirect you on your dashboard...</div>
        </div>
    </div>
    </div>
</div>
{{ partial("partials/footer") }}