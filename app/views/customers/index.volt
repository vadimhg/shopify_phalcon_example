{% extends "templates/base.volt" %}
{% block content %}
    <!-- Example Tables Card -->
    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-users"></i> Customers List
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>first_name</th>
                        <th>last_name</th>
                        <th>created_at</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>id</th>
                        <th>first_name</th>
                        <th>last_name</th>
                        <th>created_at</th>
                    </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
{% endblock %}