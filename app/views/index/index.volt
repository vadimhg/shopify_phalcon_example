<div class="content-wrapper py-3">
    <div class="container-fluid">
        <div class="jumbotron">
            {% if (shop is defined) and shop !=='' %}
                <h1>Please Wait...</h1>
                <img src="/img/loader/donut.gif">
                <script type="text/javascript">
                    window.location = '{{ url(['for': 'auth.action', 'action': 'link']) }}?shop={{ shop }}';
                </script>
            {% else %}
            <form action="{{ url(['for': 'auth.action', 'action': 'link']) }}" class="form-horizontal"  method="get">
             <div class="form-group">
                 <label for="inputShopifyName" class="col-sm-5 control-label">Please Type Your Shopify Store Name</label>
                 <div class="col-sm-10">
                   <input type="text" class="form-control" id="inputShopifyName" name="shop" value="{{ shop }}" placeholder="Shopify Store Name">
                 </div>
               </div>
               <div class="form-group">
                 <div class="col-sm-offset-2 col-sm-10">
                   <button type="submit" class="btn btn-default">Sign in</button>
                 </div>
               </div>
              </form>
          </div>
          {% endif %}
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-wrapper -->
<!-- Bootstrap core JavaScript -->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/tether/tether.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
