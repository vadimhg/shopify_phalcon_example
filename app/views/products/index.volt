{% extends "templates/base.volt" %}
{% block content %}
<style>
    td.details-control {
        background: url('/img/datatables/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.details td.details-control {
        background: url('/img/datatables/details_close.png') no-repeat center center;
    }
</style>
<!-- Example Tables Card -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-table"></i> Products List
    </div>
    <div class="card-block">
        <div class="table-responsive">
            <table class="table table-bordered" width="100%" id="dataTable" cellspacing="0">
                <thead>
                <tr>
                    <th></th>
                    <th>id</th>
                    <th>title</th>
                    <th>price</th>
                    <th>sku</th>
                    <th>position</th>
                    <th>grams</th>
                    <th>created_at</th>
                    <th>updated_at</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th></th>
                    <th>id</th>
                    <th>title</th>
                    <th>price</th>
                    <th>sku</th>
                    <th>position</th>
                    <th>grams</th>
                    <th>created_at</th>
                    <th>updated_at</th>
                </tr>
                </tfoot>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
{% endblock %}