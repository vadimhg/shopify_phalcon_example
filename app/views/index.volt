<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        {{ get_title() }}
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        {% if(assets.exists("headerCSS")) %}{{ assets.outputCss("headerCSS") }}{% endif %}
        {% if(assets.exists("headerJS")) %}{{ assets.outputJS("headerJS") }}
            <!--script type="text/javascript">
                ShopifyApp.init({
                    apiKey: '{{ SHOPIFY_API_KEY }}',
                    shopOrigin: '{{ shop }}'
                });
            </script-->
        {% endif %}
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Shopify Application">
        <meta name="author" content="Vadim Kuznetsov">
    </head>
    <body id="page-top">
        {{ flash.output() }}
        {{ content() }}
    </body>
</html>