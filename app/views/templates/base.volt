{% block navigation %}
{{ partial("partials/navigation") }}
{% endblock %}
<div class="content-wrapper py-3">
<div class="container-fluid">
    {{ partial("partials/breadcrumbs") }}
{% block content %}
    &nbsp;
{% endblock %}
</div>
<!-- /.container-fluid -->
</div>
<!-- /.content-wrapper -->
{% block footer %}
    {{ partial("partials/footer") }}
{% endblock %}