{% block top %}
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>!DOCTYPE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" style="margin:50px auto 50px auto; padding:0; width: 920px">
    <tr>
        <td style="width: 100%; height: 50px; background-color: #290252;"></td>
    </tr>
    <tr>
        <td colspan="2" style="float: right; width: 226px; height: 20px; background-color: #58b1da"></td>
    </tr>
    {% endblock %}
    {% block body %}
    {% endblock %}
    {% block bottom %}
    <tr>
        <td style="float: left; width: 198px; height: 20px; background-color: #58b1da"></td>
    </tr>
    <tr>
        <td style="width: 100%; height: 54px; background-color: #290252;"></td>
    </tr>
</table>
</body>
</html>
{% endblock %}