<?php
return [
    (object)[
        'topic'=>'orders/create',
        'address'=>'/webhook/orders_create'
    ],
    (object)[
        'topic'=>'app/uninstalled',
        'address'=>'/webhook/app_uninstalled'
    ],
    (object)[
        'topic'=>'shop/update',
        'address'=>'/webhook/shop_update'
    ]
];