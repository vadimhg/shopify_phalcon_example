<?php
/**
 * @var \Phalcon\Events\Manager $eventsManager
 */

use Phalcon\Mvc\Router;

$router = new Router();

$router->removeExtraSlashes(true);

if (!isset($_GET['_url'])) {
    $router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);
}

$router->setEventsManager($eventsManager);

$router->add('/:controller', [
    'controller' => 1,
    'action'     => 'index'
])->setName('front.controller');

/*$router->add('/contact-us', [ // todo:
    'controller' => 'contact',
    'action'     => 'index'
])->setName('front.contact');*/

$router->add('/auth/:action', [
    'controller' => 'auth',
    'action'     => 1
])->setName('auth.action');

$router->add('/:controller/:action/:params', [
    'controller' => 1,
    'action'     => 2,
    'params'     => 3,
])->setName('front.full');

return $router;
