<?php
/**
 * We're a registering a set of directories taken from the configuration file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Loader;

$loader = new Loader;

$loader->registerNamespaces(
    [
        'App\Controllers' => DOCROOT . $config->get('application')->controllersDir,
        'App\Forms'       => DOCROOT . $config->get('application')->formsDir,
        'App\Models'      => DOCROOT . $config->get('application')->modelsDir,
        'App\Plugins'     => DOCROOT . $config->get('application')->pluginsDir,
        'App\Library'     => DOCROOT . $config->get('application')->libraryDir,
        'App\Modules'     => DOCROOT . $config->get('application')->modulesDir,
    ]
);

$loader->register();


