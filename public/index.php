<?php

use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;
ini_set('display_errors', 1);
define('APP_VERSION', '0.0001');
try {
    require_once realpath(dirname(dirname(__FILE__))) . '/app/config/env.php';
    /**
     * Read the configuration
     */
    $config = new ConfigIni(APP_PATH . 'config/config.ini');
    if (is_readable(APP_PATH . 'config/config.ini.dev')) {
        $override = new ConfigIni(APP_PATH . 'config/config.ini.dev');
        $config->merge($override);
    }

    /**
     * Auto-loader configuration
     */
    require APP_PATH . 'config/loader.php';

    /**
     * Load application services
     */
    require APP_PATH . 'config/services.php';
    require APP_PATH . 'modules/UncachedErrorsCheck.php';

    $application = new Application($di);
    $application->setEventsManager($eventsManager);
    //throw new \Exception('MESSAGE');
    if (APPLICATION_ENV == APP_TEST) {
        return $application;
    } else {
        echo $application->handle()->getContent();
    }
} catch (Exception $exception){
    (new \App\Modules\ExceptionHandler)->setException($exception)->handle();
}
