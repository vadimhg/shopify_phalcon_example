(function($) {
    "use strict"; // Start of use strict

    // Scroll to top button appear
    $(document).scroll(function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        var detailRows = [];
        $('#dataTable').DataTable({
            "bServerSide": true,
            "sAjaxSource": "/datatables/customers",
            "sServerMethod": "GET",
            "columns":[
                {"data": "id", "orderable": false},
                {"data": "first_name", "orderable": false},
                {"data": "last_name", "orderable": false},
                {"data": "created_at", "orderable": false}
            ],
            "columnDefs": [{"targets": 'no-sort', "orderable": false}],
            "order":[],
            "bInfo" : false
        });
    });


})(jQuery); // End of use strict
