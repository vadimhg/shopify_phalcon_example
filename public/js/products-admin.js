(function($) {
    "use strict"; // Start of use strict

    // Scroll to top button appear
    $(document).scroll(function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });
    function format ( data ) {
         return $('<tr>').addClass("table-info")
             .append($('<td>'))
             .append($('<td>'))
             .append($('<td>').text(data.title))
             .append($('<td>').text(data.price))
             .append($('<td>').text(data.sku))
             .append($('<td>').text(data.position))
             .append($('<td>').text(data.grams))
             .append($('<td>').text(data.created_at))
             .append($('<td>').text(data.updated_at));
    }
    // Call the dataTables jQuery plugin
    $(document).ready(function() {
        var detailRows = [];
        var dt = $('#dataTable').DataTable({
            "bServerSide": true,
            "sAjaxSource": "/datatables/products",
            "sServerMethod": "GET",
            "columns":[
                    {"class":"details", "orderable":false, "data":null, "defaultContent": ""},
                    {"data": "id", "orderable": false},
                    {"data": "title", "orderable": false},
                    {"orderable":false, "data":"price", "defaultContent": "-"},
                    {"orderable":false, "data":"sku", "defaultContent": "-"},
                    {"orderable":false, "data":"position", "defaultContent": "-"},
                    {"orderable":false, "data":"grams", "defaultContent": "-"},
                    {"data": "created_at", "orderable": false},
                    {"data": "updated_at", "orderable": false},
            ],
            "columnDefs": [{"targets": 'no-sort', "orderable": false}],
            "order":[],
            "bInfo" : false,
            "createdRow": function( row, data, dataIndex ) {
                if (data.variants.length>1) {
                    $(row).children('.details').addClass('details-control');
                }
            }
        }).on('draw', function () {
            $.each( detailRows, function ( i, id ) {
                $('#'+id+' td.details-control').trigger( 'click' );
            } );
        });
        $('#dataTable tbody').on('click', 'tr td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = dt.row( tr );
            var idx = $.inArray( tr.attr('id'), detailRows );
            if ( row.child.isShown() ) {
                tr.removeClass( 'details' );
                row.child.hide();
                // Remove from the 'open' array
                detailRows.splice( idx, 1 );
            }
            else {
                tr.addClass( 'details' );
                row.child(row.data().variants.map(format)).show();
                // Add to the 'open' array
                if ( idx === -1 ) {
                    detailRows.push( tr.attr('id') );
                }
            }
        } );
    });


})(jQuery); // End of use strict
