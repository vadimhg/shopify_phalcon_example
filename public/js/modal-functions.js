/**
 * Created by vadim on 09.06.17.
 */
function modalMessageShow(title, text, type){
    var bgDict = {
        'success':'bg-success',
        'error':'bg-danger',
        'info':'bg-info'
    };
    var baseModalAlert = $('#baseModalAlert');
    if (!baseModalAlert.is(':visible')) {
        baseModalAlert.find('.modal-header').removeClass(Object.values(bgDict).join(' ')).addClass(bgDict[type]).find('.modal-title').text(title);
        baseModalAlert.find('.modal-body p').text(text);
        baseModalAlert.modal({
            keyboard: true,
            show: true,
            focus:true
        });
    }
}


