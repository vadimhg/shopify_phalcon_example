(function($) {
    "use strict"; // Start of use strict
    $(document).ready(function() {
        var steps_count = 2;
        function markStepAsFinished(step_number){
            $('.j-install-step-'+step_number).addClass('complete').find('.bs-wizard-dot').addClass('bg-success');
        }

        function markStepAsFailed(step_number){
            $('.j-install-step-'+step_number).find('.bs-wizard-dot').addClass('bg-danger');
            modalMessageShow('Error While Installing Application', 'Error On Step #'+step_number, 'error');
        }

        function step(step_number){
            if(steps_count>=step_number) {
                $.ajax({
                    "dataType": "json",
                    "type": "POST",
                    "async": true,
                    "url": '/installing_ajax/step'+step_number,
                    "contentType": "application/json",
                    "success": function (data) {
                        markStepAsFinished(step_number);
                        step(step_number + 1);
                    },
                    "error": function (data) {
                        markStepAsFailed(step_number);
                    }
                });
            } else {
                markStepAsFinished(step_number);
                window.location = "/dashboard/index";
            }
        }
        markStepAsFinished(0);
        step(1);
    });

})(jQuery); // End of use strict
